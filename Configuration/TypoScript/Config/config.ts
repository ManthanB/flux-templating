config {
	linkVars = L
	uniqueLinkVars = 1
	renderCharset = utf-8
	metaCharset = utf-8
	enableContentLengthHeader = 1
	sendCacheHeaders = 1
	baseURL = {$plugin.tx_fluxtemplating.domain}
	absRefPrefix = {$plugin.tx_fluxtemplating.absRefPrefix}
	doctype = html5
    xmlprologue = none

	# language
	htmlTag_langKey = en
	language = en_GB
	locale_all = en_GB

	sys_language_uid = 0
	sys_language_mode = content_fallback
	sys_language_overlay = hideNonTranslated

	removeDefaultJS = external

	# cross domain linking
	typolinkEnableLinksAcrossDomains = 1

	sword_standAlone = 0
	sword_noMixedCase = 0
	intTarget = _self
	extTarget = _blank
	spamProtectEmailAddresses = -2
	spamProtectEmailAddresses_atSubst = @<script type="text/javascript"> obscureAddMid() </script>
	spamProtectEmailAddresses_lastDotSubst = .<script type="text/javascript"> obscureAddEnd() </script>
	noScaleUp = 1
	no_cache = 0
	additionalHeaders = Content-Type:text/html;charset=utf-8

	content_from_pid_allowOutsideDomain = 1
	pageTitleFirst = 1

	simulateStaticDocuments = 0
	index_enable = 1
	index_externals = 0
	index_metatags = 0
	
	compressJs = {$plugin.tx_fluxtemplating.compressJs}
	concatenateJs = {$plugin.tx_fluxtemplating.concatenateJs}
	compressCss = {$plugin.tx_fluxtemplating.compressCss}
	concatenateCss = {$plugin.tx_fluxtemplating.concatenateCss}

	tx_realurl_enable = 1
}
