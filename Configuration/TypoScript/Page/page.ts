################
# page
################

# remove class bodytext
lib.parseFunc_RTE.nonTypoTagStdWrap.encapsLines.addAttributes.P.class >

page {

    bodyTagCObject = TEXT
    bodyTagCObject.data = page:uid
    bodyTagCObject.noTrimWrap = |<body id="page_id_|"> |

    meta {
        viewport = width=device-width, initial-scale=1.0
        author = 
        keywords.override.field = keywords
        description.override.field = description
        author.override.field = author
        robots = index,follow
        msapplication-square70x70logo = typo3conf/ext/fluxtemplating/Resources/Public/Images/tiny_70x70.png
        msapplication-square150x150logo = typo3conf/ext/fluxtemplating/Resources/Public/Images/square_150x150.png
        msapplication-wide310x150logo = typo3conf/ext/fluxtemplating/Resources/Public/Images/wide_310x150.png
        msapplication-square310x310logo = typo3conf/ext/fluxtemplating/Resources/Public/Images/large_310x310.png
    }
    shortcutIcon = typo3conf/ext/fluxtemplating/Resources/Public/Images/favicon.ico
    headerData {
        // Seitentitel setzen
        5 = TEXT
        5 {
            field = subtitle // title
            wrap = <title>|</title>
        }
        12 = TEXT
        12 {
            value = <meta name="robots" content="noindex" />
            if.isTrue.data = page:no_search
        }
    }  
}